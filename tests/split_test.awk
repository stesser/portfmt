BEGIN {
	RS = "<<<<<<<<<\n"
}

{
	if ($0 == "<<<empty>>>\n") {
		chunks[NR] = ""
	} else {
		chunks[NR] = $0
	}
	n++
}

END {
	if (n == 1 || n == 2) {
		printf("%s", chunks[1]) >testname ".in"
		printf("%s", chunks[2]) >testname ".expected"
	} else if (n == 3) {
		printf("%s", chunks[1]) >testname ".sh"
		printf("%s", chunks[2]) >testname ".in"
		printf("%s", chunks[3]) >testname ".expected"
	} else {
		printf("unsupported number of chunks: %d\n", n)
		exit(1)
	}
}
