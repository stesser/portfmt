# You can force skipping these test by defining IGNORE_PATH_CHECKS
.if !defined(IGNORE_PATH_CHECKS)
.if ! ${PREFIX:M/*}
.BEGIN:
	@${ECHO_MSG} "PREFIX must be defined as an absolute path so that when 'make'"
	@${ECHO_MSG} "is invoked in the work area PREFIX points to the right place."
	@${FALSE}
.endif
.endif

DATADIR?=		${PREFIX}/share/${PORTNAME}
DOCSDIR?=		${PREFIX}/share/doc/${PORTNAME}
ETCDIR?=		${PREFIX}/etc/${PORTNAME}
EXAMPLESDIR?=	${PREFIX}/share/examples/${PORTNAME}
WWWDIR?=		${PREFIX}/www/${PORTNAME}
<<<<<<<<<
(ast
 (edited? #f)
 (location 0 0)
 (comment
  (edited? #f)
  (location 1 2)
  (line "# You can force skipping these test by defining IGNORE_PATH_CHECKS"))
 (if
  (edited? #f)
  (location 2 3)
  (type #:if)
  (indent 0)
  (elseif? #f)
  (test
   (word "!")
   (word "defined(")
   (word "IGNORE_PATH_CHECKS")
   (word ")"))
  (body
   (if
    (edited? #f)
    (location 3 4)
    (type #:if)
    (indent 0)
    (elseif? #f)
    (test
     (word "!")
     (word "${PREFIX:M/*}"))
    (body
     (target
      (edited? #f)
      (location 4 5)
      (type #:named)
      (sources
       (word ".BEGIN"))
      (target-command
       (edited? #f)
       (location 5 6)
       (words
        (word "${ECHO_MSG}")
        (word ""PREFIX must be defined as an absolute path so that when 'make'""))
       (flags #:silent))
      (target-command
       (edited? #f)
       (location 6 7)
       (words
        (word "${ECHO_MSG}")
        (word ""is invoked in the work area PREFIX points to the right place.""))
       (flags #:silent))
      (target-command
       (edited? #f)
       (location 7 8)
       (words
        (word "${FALSE}"))
       (flags #:silent)))))
   (comment
    (edited? #f)
    (location 10 11)
    (line ""))
   (variable
    (edited? #f)
    (location 11 12)
    (name "DATADIR")
    (modifier #:optional)
    (words
     (word "${PREFIX}/share/${PORTNAME}")))
   (variable
    (edited? #f)
    (location 12 13)
    (name "DOCSDIR")
    (modifier #:optional)
    (words
     (word "${PREFIX}/share/doc/${PORTNAME}")))
   (variable
    (edited? #f)
    (location 13 14)
    (name "ETCDIR")
    (modifier #:optional)
    (words
     (word "${PREFIX}/etc/${PORTNAME}")))
   (variable
    (edited? #f)
    (location 14 15)
    (name "EXAMPLESDIR")
    (modifier #:optional)
    (words
     (word "${PREFIX}/share/examples/${PORTNAME}")))
   (variable
    (edited? #f)
    (location 15 16)
    (name "WWWDIR")
    (modifier #:optional)
    (words
     (word "${PREFIX}/www/${PORTNAME}"))))))
