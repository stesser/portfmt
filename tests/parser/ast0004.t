.if 1
.for a in 1 2
FOO=bar
.endfor
.for a in 1 2
FOO=bar
.endfor
.for a in 1 2
FOO=bar
.endfor
.else
MUH=muh
.endif
<<<<<<<<<
(ast
 (edited? #f)
 (location 0 0)
 (if
  (edited? #f)
  (location 1 2)
  (type #:if)
  (indent 0)
  (elseif? #f)
  (test
   (word "1"))
  (body
   (for
    (edited? #f)
    (location 2 3)
    (indent 0)
    (bindings
     (word "a"))
    (words
     (word "1")
     (word "2"))
    (body
     (variable
      (edited? #f)
      (location 3 4)
      (name "FOO")
      (modifier #:assign)
      (words
       (word "bar"))))
    (for
     (edited? #f)
     (location 5 6)
     (indent 0)
     (bindings
      (word "a"))
     (words
      (word "1")
      (word "2"))
     (body
      (variable
       (edited? #f)
       (location 6 7)
       (name "FOO")
       (modifier #:assign)
       (words
        (word "bar"))))
     (for
      (edited? #f)
      (location 8 9)
      (indent 0)
      (bindings
       (word "a"))
      (words
       (word "1")
       (word "2"))
      (body
       (variable
        (edited? #f)
        (location 9 10)
        (name "FOO")
        (modifier #:assign)
        (words
         (word "bar")))))
     (else
      (if
       (edited? #f)
       (location 11 12)
       (type #:else)
       (indent 0)
       (elseif? #t)
       (body
        (variable
         (edited? #f)
         (location 12 13)
         (name "MUH")
         (modifier #:assign)
         (words
          (word "muh")))))))))))
