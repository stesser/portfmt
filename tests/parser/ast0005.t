.if 0
.    for deptype in ${_OPTIONS_DEPENDS}
.      if defined(${opt}_${deptype}_DEPENDS)
${deptype}_DEPENDS+=	${${opt}_${deptype}_DEPENDS}
.      endif
.    endfor
.else
FOO=asd
.endif
<<<<<<<<<
(ast
 (edited? #f)
 (location 0 0)
 (if
  (edited? #f)
  (location 1 2)
  (type #:if)
  (indent 0)
  (elseif? #f)
  (test
   (word "0"))
  (body
   (for
    (edited? #f)
    (location 2 3)
    (indent 4)
    (bindings
     (word "deptype"))
    (words
     (word "${_OPTIONS_DEPENDS}"))
    (body
     (if
      (edited? #f)
      (location 3 4)
      (type #:if)
      (indent 6)
      (elseif? #f)
      (test
       (word "defined(")
       (word "${opt}_${deptype}_DEPENDS")
       (word ")"))
      (body
       (variable
        (edited? #f)
        (location 4 5)
        (name "${deptype}_DEPENDS")
        (modifier #:append)
        (words
         (word "${${opt}_${deptype}_DEPENDS}"))))))
    (else
     (if
      (edited? #f)
      (location 7 8)
      (type #:else)
      (indent 0)
      (elseif? #t)
      (body
       (variable
        (edited? #f)
        (location 8 9)
        (name "FOO")
        (modifier #:assign)
        (words
         (word "asd"))))))))))
