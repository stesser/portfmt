;; Per-directory local variables for GNU Emacs 23 and later.

((c-mode
  .
  ((eval . (font-lock-add-keywords 'c-mode '(("\\<\\(panic\\|\\|panic_if\\|panic_unless\\|unless\\|ARRAY_FOREACH\\|ARRAY_FOREACH_SLICE\\|FRAMEMAP_FOREACH\\|FRAMEMAP_FOREACH_SLICE\\|MAP_FOREACH\\|MAP_FOREACH_SLICE\\|QUEUE_FOREACH\\|QUEUE_FOREACH_SLICE\\|SCOPE_MEMPOOL\\|SET_FOREACH\\|SET_FOREACH_SLICE\\|STACK_FOREACH\\|STACK_FOREACH_SLICE\\|LINE_FOREACH\\|LINE_FOREACH_SLICE\\|DIRECTORY_FOREACH\\|DIRECTORY_FOREACH_SLICE\\)\\>" . font-lock-keyword-face))))
   (c-file-style . "BSD")))
 (scheme-mode
  .
  ((indent-tabs-mode . nil)
   (eval . (put 'modify-inputs 'scheme-indent-function 1))
   (eval . (put 'modify-phases 'scheme-indent-function 1))
   (eval . (put 'replace 'scheme-indent-function 1))
   (eval . (put 'add-before 'scheme-indent-function 2))
   (eval . (put 'add-after 'scheme-indent-function 2))
   (eval . (put 'package 'scheme-indent-function 0))
   (eval . (put 'package/inherit 'scheme-indent-function 1))
   (eval . (put 'origin 'scheme-indent-function 0))
   (eval . (put 'build-system 'scheme-indent-function 0))
   (eval . (put 'bag 'scheme-indent-function 0))
   (eval . (put 'gexp->derivation 'scheme-indent-function 1))
   (eval . (put 'graft 'scheme-indent-function 0))

   ;; This notably allows '(' in Paredit to not insert a space when the
   ;; preceding symbol is one of these.
   (eval . (modify-syntax-entry ?~ "'"))
   (eval . (modify-syntax-entry ?$ "'"))
   (eval . (modify-syntax-entry ?+ "'")))))
